package com.andrewrp.movie.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.andrewrp.movie.R;

import org.w3c.dom.Text;

public class MovieHolder extends RecyclerView.ViewHolder {

    ImageView imageView;
    TextView mtitle;

    public MovieHolder(@NonNull View itemView) {
        super(itemView);

        imageView = itemView.findViewById(R.id.imgPoster);
        mtitle = itemView.findViewById(R.id.mTitle);
    }
}
