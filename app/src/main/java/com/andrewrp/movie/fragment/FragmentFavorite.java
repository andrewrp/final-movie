package com.andrewrp.movie.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.andrewrp.movie.DBFavorite.AppRoomDatabase;
import com.andrewrp.movie.DBFavorite.FavoriteDB;
import com.andrewrp.movie.R;
import com.andrewrp.movie.adapter.FavoriteAdapter;
import com.andrewrp.movie.data.model.Favorite;

import org.w3c.dom.Text;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentFavorite extends Fragment {

    ImageView imageView;
    TextView title;
    RecyclerView recyclerView;
    ProgressBar progressBar;

    FavoriteAdapter mAdapter;
    List<Favorite> favorites;


    public FragmentFavorite() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_movie, container, false);
        imageView = view.findViewById(R.id.imgPoster);
        title = view.findViewById(R.id.mTitle);
        recyclerView = view.findViewById(R.id.listMovie);
        progressBar = view.findViewById(R.id.progresssBar);
        progressBar.setVisibility(View.VISIBLE);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        favorites = FavoriteDB.favDB.favoriteDao().getAll();
        mAdapter = new FavoriteAdapter(favorites);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        recyclerView.setAdapter(mAdapter);
        progressBar.setVisibility(View.GONE);
    }
}
