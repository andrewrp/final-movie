package com.andrewrp.movie.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andrewrp.movie.R;
import com.andrewrp.movie.data.Api;
import com.andrewrp.movie.data.model.Favorite;
import com.andrewrp.movie.utils.DownloadImage;

import java.util.List;

public class FavoriteAdapter extends RecyclerView.Adapter<MovieHolder> {
    private List<Favorite> filmFavorites;
    private ListOnClick mlistener;

    public FavoriteAdapter(List<Favorite> filmFavorite) {
        this.filmFavorites = filmFavorite;
    }

    @NonNull
    @Override
    public MovieHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_adapter, parent, false);

        MovieHolder holder = new MovieHolder(view);
        mlistener = (ListOnClick) parent.getContext();
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MovieHolder holder, int position) {
        final Favorite filmFavorite = filmFavorites.get(position);
        holder.mtitle.setText(filmFavorite.getTitle());
        DownloadImage.picasso(Api.POSTER_PATH + filmFavorites.get(position).getPoster(),
                holder.imageView);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mlistener != null) {
                    mlistener.mClick(filmFavorite);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return filmFavorites.size();
    }

    public interface ListOnClick {
        void mClick(Favorite favorite);
    }
}
