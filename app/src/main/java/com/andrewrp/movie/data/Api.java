package com.andrewrp.movie.data;

public class Api {

    private static String KEY = "ba28a14bf85f7b2c4b706b3054babe1e";
    private static String BASE_URL = "http://api.themoviedb.org/3/";
    public static String MOVIE_POPULAR = BASE_URL + "movie/popular?api_key=" + KEY + "&language=en-US";
    public static String MOVIE_UPCOMING = BASE_URL + "movie/upcoming?api_key=" + KEY + "&language=en-US";
    public static String MOVIE_VIDEO = BASE_URL + "movie/videos?api_key=" + KEY + "&language=en-US";
    public static String POSTER_PATH = "https://image.tmdb.org/t/p/w600_and_h900_bestv2/";

    public static String MOVIE_VIDEO_BASE = BASE_URL + "movie/";
    public static String MOVIE_VIDEO_URL = "/videos?api_key=" + KEY + "&language=en-US";
    public static String MOVIE_DESC = "?api_key=" + KEY + "&language=en-US";

}
