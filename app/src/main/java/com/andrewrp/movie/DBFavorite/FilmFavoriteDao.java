package com.andrewrp.movie.DBFavorite;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.andrewrp.movie.data.model.Favorite;

import java.util.List;

@Dao
public interface FilmFavoriteDao {

    @Query("Select * From Favorite")
    List<Favorite> getAll();

    @Query("Select * From Favorite where tmdbId = :id")
    Favorite selectOneRow(String id);

    //Crud
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Favorite favorite);

    @Update
    void update(Favorite favorite);

    @Delete
    void delete(Favorite favorite);
}
