package com.andrewrp.movie.utils;

import android.widget.ImageView;

import com.andrewrp.movie.R;
import com.squareup.picasso.Picasso;

public class DownloadImage {

    public static void picasso(String url, ImageView imageView) {
        Picasso.get().load(url)
                .placeholder(R.drawable.ic_launcher_background)
                .error(R.drawable.ic_launcher_foreground)
                .fit().centerCrop().into(imageView);
    }
}
