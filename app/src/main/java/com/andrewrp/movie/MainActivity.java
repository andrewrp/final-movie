package com.andrewrp.movie;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.andrewrp.movie.adapter.FavoriteAdapter;
import com.andrewrp.movie.adapter.MovieAdapter;
import com.andrewrp.movie.adapter.ViewPagerAdapter;
import com.andrewrp.movie.data.model.Favorite;
import com.andrewrp.movie.data.model.Movie;
import com.andrewrp.movie.fragment.FragmentFavorite;
import com.andrewrp.movie.fragment.FragmentPopular;
import com.andrewrp.movie.fragment.FragmentUpcoming;

import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements MovieAdapter.ListOnClick, FavoriteAdapter.ListOnClick {

    public static ViewPager viewPager;
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewPager = findViewById(R.id.viewPager);
        tabLayout = findViewById(R.id.tabLayout);

        tabs(viewPager);
        tabLayout.setupWithViewPager(viewPager);

    }

    private void tabs(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new FragmentPopular(), "Popular");
        adapter.addFragment(new FragmentUpcoming(), "UpComing");
        adapter.addFragment(new FragmentFavorite(), "Favorite");
        viewPager.setAdapter(adapter);
    }

    @Override
    public void mClick(Movie movie) {
        Intent i = new Intent(MainActivity.this, MovieDetail.class);

        i.putExtra("title", String.valueOf( movie.getTitle()));
        i.putExtra("image", String.valueOf( movie.getPoster()));
        i.putExtra("ID", String.valueOf( movie.getId()));
        i.putExtra("synopsis",String.valueOf( movie.getSynopsis()));
        i.putExtra("release_date",String.valueOf( movie.getRelease_date()));
        i.putExtra("rating",String.valueOf( movie.getRating()));
        startActivity(i);

    }

    @Override
    public void mClick(Favorite favorite) {
        Intent i = new Intent(MainActivity.this, MovieDetail.class);
        i.putExtra("title", String.valueOf(favorite.getTitle()));
        i.putExtra("image", String.valueOf(favorite.getPoster()));
        i.putExtra("ID", String.valueOf(favorite.getTmdbId()));
        startActivity(i);
    }

}
