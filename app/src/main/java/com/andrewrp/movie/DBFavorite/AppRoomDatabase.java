package com.andrewrp.movie.DBFavorite;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.andrewrp.movie.data.model.Favorite;

    @Database(entities = {Favorite.class}, version = 2)
    public abstract class AppRoomDatabase extends RoomDatabase{
        public abstract FilmFavoriteDao favoriteDao();
    }
