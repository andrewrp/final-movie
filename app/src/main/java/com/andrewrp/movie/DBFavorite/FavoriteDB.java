package com.andrewrp.movie.DBFavorite;

import android.app.Application;
import android.arch.persistence.room.Room;

public class FavoriteDB extends Application{
    public static AppRoomDatabase favDB;

    @Override
    public void onCreate() {
        super.onCreate();

        favDB = Room.databaseBuilder(getApplicationContext(),
                AppRoomDatabase.class,"filmfavorite.db")
                .allowMainThreadQueries().build();
    }
}
